import init, { plot_function } from '@wanim/wanim-rs';

(async () => {
  await init();

  const inp: HTMLInputElement | null = document.querySelector("#expression-input");
  if (inp === null) {
    console.error("Unable to get input element");
    return;
  }

  const canvas: HTMLCanvasElement | null = document.querySelector("#plot-canvas");
  if (canvas === null) {
    console.error("Unable to get canvas element");
    return;
  }

  const context: CanvasRenderingContext2D | null = canvas.getContext("2d");
  if (context === null) {
    console.error("Unable to get context from canvas");
    return;
  }

  inp.addEventListener('change', () => {
    plot_function(inp.value, context);
  });
})();
